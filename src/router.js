import { createRouter, createWebHistory } from 'vue-router';

import Home from './components/Home.vue';
import ProjectLayout from './components/ProjectLayout.vue';
import Portfolio from './components/projectСases/Portfolio.vue';
import Funfinder from './components/projectСases/Funfinder.vue';
import Todolist from './components/projectСases/Todolist.vue';
import NotFound from './components/NotFound.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/project',
    name: 'Project',
    component: ProjectLayout,
    children: [
      {
        path: 'portfolio',
        name: 'Portfolio',
        component: Portfolio,
      },

      {
        path: 'funfinder',
        name: 'Funfinder',
        component: Funfinder,
      },

      {
        path: 'todolist',
        name: 'Todolist',
        component: Todolist,
      },

      { path: '*', component: NotFound },
    ],
  },
];
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
