# Portfolio

> **I just really wanted to see all my projects in one place. Now you can, too &#128105;&#8205;&#128187;**

Live demo you can watch [here](https://arkhipova.gitlab.io/portfolio/). <br />

## What else do I want to do

- [x] ~~Remove the "crutch" way of routing in GitLab Pages using Hash Mode~~
- [x] ~~Add "where I worked" section~~
- [ ] Write an animation for transition between pages
- [ ] Add a detailed description of the projects
- [ ] Remove everything unnecessary by following the DRY principle

## Build Setup

```bash
# clone repo
git clone https://gitlab.com/Arkhipova/portfolio.git

# install dependencies
npm i

# serve with hot reload at http://localhost:8080/
npm run serve

# build for production with minification
npm run build
```

## Explanation of characters in commits

`+` — added

`-` — deleted

`$` — updated

<br />

_The design of the project was made in [Figma](https://www.figma.com/file/m3J9RrwwmpWCPA2Q2AxbEH/Portfolio?node-id=0%3A1)._
